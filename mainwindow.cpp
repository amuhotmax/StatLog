#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>


#include <iostream>

#include <QFuture>
#include <QtConcurrent/QtConcurrent>

using namespace std;
using namespace QtConcurrent;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
 //   ui->lineEdit_2->setText(QDir::homePath());
    connect(&this->FutureWatcher, SIGNAL (finished()), this, SLOT (slot_finished()));
    connect(&this->FutureWatcher, SIGNAL (started()), this, SLOT (slot_busy()));
    ui->label->setText("");
    ui->progressBar->setValue(0);
    ui->label_warning->setText("");

    ui->tableWidget->setRowCount(10);
    ui->tableWidget->setColumnCount(11);
  //  QStringList m_TableHeader;
    //m_TableHeader<<"filename"<<"totaltime"<<"creates"<<"sets"<<"adds"<<"movefroms"<<"movetos"<<"moveafters"<<"removes"<<"readings"<<"modelings";
   // ui->tableWidget->setHorizontalHeaderLabels(m_TableHeader);
   // ui->tableWidget->setItem(0, 1, new QTableWidgetItem("Hello"));
    QStringList loadCsv;
    //loadCsv<<"een"<<"twee"<<"drie";
   // QTableWidgetItem *items= new QTableWidgetItem(loadCsv.at(0));
    //ui->tableWidget->setItem((0), 0, items);
    ui->tableWidget->hide();
    ui->lineEdit->setText("/Users/dave/Google Drive/PhD Research Dave/2015/WebUML/Makarere Experiment/Second Round/all files");
    dir = ui->lineEdit->text();
    ui->lineEdit_2->setText(QDir::homePath()+"/stats-"+QDate::currentDate().toString("MMddyy")+"-"+QTime::currentTime().toString()+".csv");
}

void MainWindow::slot_finished()
{
 ui->progressBar->setValue(100);
 ui->label->setText("amalysis exported to outputfile");
 ui->pushButton_3->setText("Generate Statistics");
}

void MainWindow::slot_busy()
{
 ui->progressBar->setValue(50);
 ui->label->setText("analysinglog files ...");
 ui->pushButton_3->setText("Generating");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"), QDir::homePath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    this->ui->lineEdit->setText(dir);
}


void MainWindow::on_pushButton_2_clicked()
{
   // QFileDialog dialog(this);
    ui->lineEdit_2->setText(QFileDialog::getSaveFileName(this, "Save CSV File",QDir::homePath()+"/stats.csv", "CSV (*.csv)"));

}

void MainWindow::on_pushButton_3_clicked()
{
    QFileInfo fi1(dir);


  if (fi1.isDir() &&  ui->lineEdit_2->text()!=" no outputfile set yet" ){
        QString pars (dir+"|"+ui->lineEdit_2->text());
        QFuture<void> future = QtConcurrent::run(callStats, pars.toStdString());
        this->FutureWatcher.setFuture(future);
  }
  else{
      ui->label_warning->setText("input or output not set");
  }
}
