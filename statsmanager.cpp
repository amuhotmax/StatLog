#include "statsmanager.h"
#include <dirent.h>
#include <sys/stat.h>
#include <time.h>
#include <QStringList>
#include <regex>
#include <QXmlStreamReader>
#include <QFile>
#include <QMessageBox>
#include <QDebug>

void callStats(string pars){
    StatsManager s;
    clock_t t;

    QString par = QString::fromStdString(pars);
    QStringList params = par.split("|");
    string filename = params[0].toStdString();

    ofstream outputfile;
    outputfile.open(params[1].toStdString());
    cout<<params[1].toStdString();
    int analysedfiles=0;
    int errorfiles=0;
    DIR *dpdf;
    struct dirent *epdf;
    string folderName = filename;
    dpdf = opendir(folderName.c_str());
    t = clock();
    if (dpdf != NULL){

        outputfile<<"filename"<<","<<"totaltime"<<","<<"creates"<<","<<"sets"<<","<<"adds"<<","<<"movefroms"<<","<<"movetos"<<","<<"moveafters";
        outputfile<<","<<"removes"<<","<<"readings"<<","<<"modelings"<<","<<"comments"<<","<<"lines"<<endl;

        while (epdf = readdir(dpdf)){
            string current = ".";
            string previous ="..";
            if(epdf->d_type==DT_DIR)
                if( current.compare(epdf->d_name) !=0)
                    if( previous.compare(epdf->d_name) !=0){
                        if(s.showStats( folderName+"/"+epdf->d_name+"/log.csv" , outputfile ))
                            analysedfiles++;
                        else{
                            cout<<"wrong kind of folder: "<<epdf->d_name<<endl;
                            errorfiles++;
                        }
                        /*if(s.showXMIStats( folderName+"/"+epdf->d_name+"/model.xmi" )){
                            //analysedfiles++;
                        }
                        else{
                            cout<<"wrong kind of folder: "<<epdf->d_name<<endl;
                           // errorfiles++;
                        }*/
                    }

        }

    }

    outputfile.close();

    t = clock()-t;
    cout<<"files analysed: "<<analysedfiles<<endl;
    cout<<"files wrong: "<<errorfiles<<endl;
    cout<<"processor time: "<<((float)t)/CLOCKS_PER_SEC;
}

StatsManager::StatsManager(){
    reset();
}

void StatsManager::reset(){
    creates=0;
    adds=0;
    movetos=0;
    moveafters=0;
    movefroms=0;
    removes=0;
    readings=0;
    sets=0;
    modelings=0;
    totaltime=0;
    myMap.clear();
    line="";
    lines=0;
    comments=0;
}


bool StatsManager::showXMIStats(string filename){
    QFile xmlFile(filename.c_str());
            if (!xmlFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
                    qDebug() << filename.c_str() <<"Load XML File Problem","Couldn't open xmlfile.xml to load settings for download";
                    return false;
            }
   QXmlStreamReader xmlReader(filename.c_str());


    //Parse the XML until we reach end of it
    while(!xmlReader.atEnd() && !xmlReader.hasError()) {
            // Read next element
            QXmlStreamReader::TokenType token = xmlReader.readNext();
            //If token is just StartDocument - go to next
          /*  if(token == QXmlStreamReader::StartDocument) {
                    continue;
            }
            //If token is StartElement - read it
            if(token == QXmlStreamReader::StartElement) {

                    if(xmlReader.name() == "name") {
                            continue;
                    }

                    if(xmlReader.name() == "id") {
                        qDebug() << xmlReader.readElementText();
                    }
            }*/
    }

    if(xmlReader.hasError()) {
            qDebug() <<  "xmlFile.xml Parse Error"<< xmlReader.errorString();
            return false;
    }

    //close reader and flush file
    xmlReader.clear();
    xmlFile.close();
}


bool StatsManager::showStats(string filename, ofstream& outfile){
   // cout<<filename;
    string file;
    ifstream myfile( filename.c_str() );

          bool isEmpty = myfile.peek() == EOF;

          if (myfile && !isEmpty)  // same as: if (myfile.good())
            {
            while (getline( myfile, line ))  // same as: while (getline( myfile, line ).good())
              {
                if(line.find(",")!=string::npos){
                    myMap.insert( pair<unsigned long,string>( atol(line.substr(0,line.find(",")).c_str()) , line.substr(line.find(","), line.length()) ) );
                    lines++;
                }
                regex e1("[0-9]+,[0-9]{1,3}");
                if (regex_search (line,e1)){
                                comments++;
                                file=filename;
                                file.erase(std::remove(file.begin(), file.end(), ','), file.end());
                                cout<<file<<", "<<line<<endl;
                }

              }

            for(map<unsigned long,string>::iterator i = myMap.begin(); i != myMap.end(); i++){
                //cout<<i->first<<i->second;


                if( i->second.find("CREATE") != std::string::npos){
                        creates++;
                }

                if( i->second.find("ADD") != std::string::npos){
                                adds++;
                               // if (filename.find("x00007") != std::string::npos)
                                //    cout<<i->second<<endl;
                }

                if( i->second.find("MOVE FROM") != std::string::npos){
                            movefroms++;
                            if( i != myMap.begin() )
                               if( (std::prev(i))->second.find("CREATE") != std::string::npos )
                                    moveafters++;
                }

                if( i->second.find("MOVE TO") != std::string::npos){
                                movetos++;
                }

                if( i->second.find("REMOVE") != std::string::npos)
                                removes++;
                if( i->second.find("SET") != std::string::npos)
                                                    sets++;
                if( i->second.find("Reading") != std::string::npos)
                                        readings++;
                if( i->second.find("Modeling") != std::string::npos)
                                        modelings++;

            }

            map<unsigned long,string>::iterator it = myMap.end();
            it--;
            totaltime = it->first - myMap.begin()->first;

//            cout<<"statistics "<<filename<<endl;
//            cout<<"--------------------------------------------"<<endl;
//            cout<<"Totale tijd is: \t\t"<<totaltime/60000<<" minutes"<<endl;
//            cout<<"het aantal CREATE is: \t\t"<<creates<<endl;
//            cout<<"het aantal SET is: \t\t"<<sets<<endl;
//            cout<<"het aantal ADD is: \t\t"<<adds<<endl;
//            cout<<"het aantal MOVE FROM is: \t"<<movefroms<<endl;
//            cout<<"het aantal MOVE TO is: \t\t"<<movetos<<endl;
//            cout<<"het aantal MOVE AFTER is: \t"<<moveafters<<endl;
//            cout<<"het aantal REMOVES is: \t\t"<<removes<<endl;
//            cout<<"het aantal Reading is: \t\t"<<readings<<endl;
//            cout<<"het aantal Modeling is: \t"<<modelings<<endl;

            filename.erase(std::remove(filename.begin(), filename.end(), ','), filename.end());
           // filename.erase(std::remove(filename.begin(), filename.end(), '/log.cvs'), filename.end());

            outfile<<filename<<","<<totaltime/60000<<","<<creates<<","<<sets<<","<<adds<<","<<movefroms<<","<<movetos<<","<<moveafters;
            outfile<<","<<removes<<","<<readings<<","<<modelings<<","<<comments<<","<<lines<<endl;
            reset();
            myfile.close();
            return true;
            }
          else return false;

}
